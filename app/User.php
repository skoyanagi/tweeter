<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use App\Tweet;
use App\Follow;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'screen_name', 'email', 'password', 'description',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function comments() 
    {
        return $this->hasMany('\App\Comment');
    }

    public function tweets()
    {
        return $this->hasMany('\App\Tweet');
    }

    public function followers()
    {
        return $this->hasNany('\App\Follow');
    }

    
    public static function tweetCount($user_id)
    {
        return Tweet::where('user_id','=', $user_id)->get()->count();
    }

    public static function followCount($user_id)
    {
        return Follow::where('follower_id','=', $user_id)->get()->count();
    }

    public static function followersCount($user_id)
    {
        return Follow::where('following_id','=', $user_id)->get()->count();
    }

    public static function getListOfFollowers($user_id)
    {        
        $followers = Follow::where('follower_id','=', $user_id)->get();
        $users = '';
        foreach($followers as $follow)
        {
            $userName = User::find($follow->following_id)->name;
            $users = $users . $userName . "<br>";
        }
        
        return $users;
    }

    public static function getListOfPeopleFollowingMe($user_id)
    {        
        $followers = Follow::where('following_id','=', $user_id)->get();
        $users = '';
        foreach($followers as $follow)
        {
            $userName = User::find($follow->follower_id)->name;
            $users = $users . $userName . "<br>";
        }
        
        return $users;
    }

}
