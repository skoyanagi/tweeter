<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Follow extends Model
{
    protected $fillable = [
        'follower_id',
        'following_id',
    ];

    public function userFollower()
    {
        return $this->belongsTo('\App\User','follower_id');
    }

	public function userFollowing()
    {
        return $this->belongsTo('\App\User','following_id');
    }    

    public static function isFollowing($follower_id, $following_id)
    {
    	return Follow::where([ ['follower_id', '=', $follower_id], ['following_id', '=', $following_id],])->get()->count();    	
    }

}
