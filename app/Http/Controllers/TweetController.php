<?php

namespace App\Http\Controllers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\User;
use App\Tweet;
use App\Comment;
use App\Like;

class TweetController extends Controller
{

     

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {

        $user = User::findorfail($id);

        $data['user'] = $user;
        return view('tweet.create', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $tweet = new Tweet();
        $tweet->user_id = $request->input('user_id');
        $tweet->tweet_text = $request->input('tweetText');
                
        if ($tweet->save()) {
            return redirect()->home();
            
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        /*$user = User::findorfail($id);

        $data['user'] = $user;
        return view('tweet.create', $data);*/
        $tweet = Tweet::findorfail($id);
        $data['tweet'] = $tweet;
        return view('tweet.tweet');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $tweet = Tweet::findorfail($id);

        $data['tweet'] = $tweet;
        return view ('tweet.edit', $data);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $tweet = Tweet::findorfail($request->tweet_id);
        $tweet->tweet_text = $request->input('tweetText');

        if ($tweet->save()) {
            return back();
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $tweet = Tweet::findorfail($request->tweet_id);
        
        /* Delete all comments on the tweet */
        if ($tweet->comments()->count() > 0) {
            $comments = Comment::where('tweet_id','=', $request->tweet_id)->get();
            foreach( $comments as $comment) {
                $comment->delete();
            }                
        }
        
        /* Delete all likes on the tweet */
        if ($tweet->likes()->count() > 0) {
            $likes = Like::where('tweet_id','=', $request->tweet_id)->get();
            foreach($likes as $like) {
                $like->delete();
            }
        }
        
        $tweet = Tweet::findorfail($request->tweet_id)->delete();

        return redirect()->home();

        
    }

}
