<?php

namespace App\Http\Controllers;

use Validator;
use Illuminate\Http\Request;
use \App\Tweet;
use \App\Comment;
use App\User;

class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $tweet = Tweet::findorfail($id);

        $data['tweet'] = $tweet;

        return view('comment.create',$data);

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $comment = new Comment();
        $comment->user_id = $request->input('user_id');
        $comment->tweet_id = $request->input('tweet_id');
        $comment->comment = $request->input('commentText');
                
        if ($comment->save()) {
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {        
        $tweet = Tweet::findorfail($id);        
        
        $data['tweet'] = $tweet;
        

        return view('comment.show',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request)
    {
        $comment = Comment::findorfail($request->id);

        $data['comment'] = $comment;

        return view('comment.edit',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {        
        $comment = Comment::findorfail($request->comment_id);
        $comment->comment = $request->commentText;

        if($comment->save()) {
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $comment = Comment::findorfail($request->comment_id)->delete();
        return redirect()->back();
    }
}
