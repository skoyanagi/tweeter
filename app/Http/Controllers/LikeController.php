<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use App\Like;

class LikeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function likeTweet($tweetId)
    {        
        $tweet = Like::where([['tweet_id', '=', $tweetId],['user_id', '=', Auth::user()->id],] )->first();

        if (is_null($tweet))
        {
            $like = new Like;
            $like->tweet_id = $tweetId;
            $like->user_id = Auth::user()->id;

            if ($like->save())
            {
                return redirect()->home();
            }
        }
    }

    public function unlikeTweet($tweetId)
    {

        $like = Like::where([['tweet_id', '=', $tweetId],['user_id', '=', Auth::user()->id],] )->get();
        
        if($like->count() == 1) {
            $like[0]->delete();
        }
        
        return redirect()->home();        
        
    }
}
