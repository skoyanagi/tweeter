<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
	return view('index');
    //return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/logout', 'Auth\LoginController@logout')->name('logout');
Route::get('/user/{id}/edit', 'UserController@edit');
Route::get('/user/{id}', 'UserController@delete');
Route::post('/user','UserController@update');
Route::get('/user','UserController@show');

/* Tweet */
Route::get('/tweet/{id}','TweetController@create');
Route::post('/tweet','TweetController@store')->name('tweet.store');
Route::get('/tweet/like/{id}','TweetController@likeTweet');
Route::get('/tweet/{id}/delete','TweetController@destroy')->name('tweet.destroy');
Route::get('/tweet/{id}/edit','TweetController@edit');
Route::post('/tweet/{id}','TweetController@update')->name('tweet.update');
Route::post('/retweet','TweetController@retweet')->name('tweet.retweet');
Route::get('/{id}','TweetController@show')->name('tweet.show');

/* Retweet */
Route::post('/retweet','RetweetController@store')->name('retweet.store');

/* Comment */
Route::get('/comment/{id}','CommentController@create');
Route::post('/comment','CommentController@store')->name('comment.store');
Route::get('/comment/{id}/delete','CommentController@destroy')->name('comment.destroy');
Route::get('/comment/{id}/edit','CommentController@edit')->name('comment.edit');
Route::post('/comment/{id}','CommentController@update')->name('comment.update');
Route::get('/comment/{id}','CommentController@show')->name('comment.show');

/* Follow */
Route::post('/follow', 'FollowController@store');
Route::post('/unfollow', 'FollowController@delete');
Route::get('/follow/{id}', 'FollowController@show');

/* Like */
Route::get('/like/{id}','LikeController@likeTweet');
Route::get('/like/{id}/delete','LikeController@unlikeTweet');


/* Create default data */
Route::get('/createusers/', 'UserController@createusers');
