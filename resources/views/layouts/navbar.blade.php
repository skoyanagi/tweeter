<!-- Fixed navbar -->
<div class="navbar navbar-inverse navbar-fixed-top">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			    <span class="icon-bar"></span>
			 </button>
		
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav navbar-right">
				<li class="active"><a href="{{ url('home') }}">HOME</a></li>
				
				@guest
					<li><a href="{{ url('/login') }}">LOGIN</a></li>
					<li><a href="{{ url('/register') }}">REGISTER</a></li>
				@else
					<li><a href="{{ url('/logout') }}">LOGOUT</a></li>
				@endguest
			</ul>
		</div>
	</div>
</div>