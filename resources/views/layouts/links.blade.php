
<!-- Favicons -->
<!--<link href="{{ url('/') }}/img/favicon.png" rel="icon">-->
<!--<link href="img/apple-touch-icon.png" rel="apple-touch-icon">-->

<!-- Google Fonts -->
<link href="https://fonts.googleapis.com/css?family=Lato:300,400,700,900|Raleway:400,300,700,900" rel="stylesheet">

<!-- Libraries CSS Files -->
<link href="{{ url('/') }}/lib/font-awesome/css/font-awesome.min.css" rel="stylesheet">

<!-- Bootstrap CSS File -->
<link href="{{ url('/') }}/lib/bootstrap/css/bootstrap.min.css" rel="stylesheet">

<!-- Custom Stylesheet File -->
<link href="{{ url('/') }}/css/custom.css" rel="stylesheet">

<!-- Main Stylesheet File -->
<link href="{{ url('/') }}/css/style.css" rel="stylesheet">

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>