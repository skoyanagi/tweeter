<!-- JavaScript Libraries -->
<script src="{{ url('/') }}/lib/jquery/jquery.min.js"></script>
<script src="{{ url('/') }}/lib/bootstrap/js/bootstrap.min.js"></script>
<script src="{{ url('/') }}/lib/php-mail-form/validate.js"></script>
<script src="{{ url('/') }}/lib/chart/chart.js"></script>


<!-- Template Main Javascript File -->
<script src="{{ url('/') }}/js/main.js"></script>
