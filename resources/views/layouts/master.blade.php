<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
  <title>{{ config('app.name', 'Tweeter') }}</title>
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <meta content="" name="keywords">
  <meta content="" name="description">

  @include('layouts.links')

  <!-- =======================================================
    Template Name: Spot
    Template URL: https://templatemag.com/spot-bootstrap-freelance-template/
    Author: TemplateMag.com
    License: https://templatemag.com/license/
  ======================================================= -->
</head>

<body>

  @include('layouts.navbar')

  <div id="headerwrap">
    <div class="container">
      <div class="row centered">
          <div class="col-lg-10 col-lg-offset-1">            
            @yield('title')
            @yield('login')
            @yield('register')
            @yield('main')
            @yield('user-profile')
            @yield('tweet')
            @yield('comment')
            @yield('follow')
            @yield('retweet')
          </div> 
      </div>
    </div>
  </div>
<!-- headerwrap -->
  
  @include('layouts.footer')

  <div id="copyrights">
    <div class="container">
      <p>
        &copy; Copyrights <strong>Spot</strong>. All Rights Reserved
      </p>
      <div class="credits">
        <!--
          You are NOT allowed to delete the credit link to TemplateMag with free version.
          You can delete the credit link only if you bought the pro version.
          Buy the pro version with working PHP/AJAX contact form: https://templatemag.com/spot-bootstrap-freelance-template/
          Licensing information: https://templatemag.com/license/
        -->
        Created with Spot template by <a href="https://templatemag.com/">TemplateMag</a>
      </div>
    </div>
  </div>

  @include('layouts.scripts')

</body>
</html>
