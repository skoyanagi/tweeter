<?php
	use App\Follow;
?>

@extends('layouts.master')
@section('follow')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <br><br><br>
                <div class="card-header text-white bg-primary"><h2>User Profile</h2></div>
                <div class="card-body bg-info">
                    <br>
                    <div>
                    @if(!is_null($user->profile_filename))
                        <img src="{{asset('storage/upload/'.$user->profile_filename)}}" class="user-image img-responsive"/>
                        @else
                            <img src="{{ url('/')}}/img/find_user.png" class="user-image img-responsive"/>
                        @endif
                    </div>

                    <form>
                        @csrf
                        <input type="hidden" name="id" value="{{ ( old('id') ) ? old('id') : $user->id }}">
                        <br>                    	
                        
                        <div class="form-group row">
                            <label for="screenName" class="col-md-4 col-form-label text-md-right">{{ __('Screen Name') }}</label>

                            <div class="col-md-6">
                                <input id="screenName" type="text" class="form-control{{ $errors->has('screenName') ? ' is-invalid' : '' }}" name="screenName" value="{{ ( old('screenName') ) ? old('screenName') : $user->screen_name }}" required autofocus disabled>

                                @if ($errors->has('screenName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('screenName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">         
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-6">
                                <textarea id="description" rows="6" class="form-control" name="description" disabled>{{ $user->description }}</textarea>   
                            </div>
                        </div>
                        <br>
                    </form>

                    <div>
                     	<div class="form-group row">
	                        <div class="form-group row mb-1">
	                            <div class="col-md-12">
	                            	@if(Follow::isFollowing(Auth::user()->id, $user->id) == 0)
	                            	<form method="post", action="{{ url('follow/' ) }}">
	                            		@csrf
	                            		<input type="hidden" name="following_id" value="{{  $user->id }}">
	                            		<input type="hidden" name="follower_id" value="{{  Auth::user()->id }}">
			                            <button type="submit" class="btn btn-lg btn-primary">Follow</button>
                                        <a href="{{ url('home') }}" class="btn btn-lg btn-primary">Close</a>
	                                </form>
	                                @else
	                                	<form method="post", action="{{ url('unfollow/' ) }}">
	                            		@csrf
	                            		<input type="hidden" name="following_id" value="{{  $user->id }}">
	                            		<input type="hidden" name="follower_id" value="{{  Auth::user()->id }}">
	                            		<button type="submit" class="btn btn-lg btn-primary">UnFollow</button>
                                        <a href="{{ url('home') }}" class="btn btn-lg btn-primary">Close</a>
	                                </form>
	                                @endif
	                            </div>
	                        </div>
	                    </div>
	                </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection