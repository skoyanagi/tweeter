@extends('layouts.master')
@section('user-profile')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <br><br><br>
                <div class="card-header text-white bg-primary"><h2>User Profile</h2></div>
                <div class="card-body bg-info">
                    <br>
                    <div>
                    @if(!is_null($user->profile_filename))
                        <img src="{{asset('storage/upload/'.$user->profile_filename)}}" class="user-image img-responsive"/>
                        @else
                            <img src="{{ url('/')}}/img/find_user.png" class="user-image img-responsive"/>
                        @endif
                    </div>
                    <form method="POST" action="{{ url('/user') }}" enctype="multipart/form-data">
                        @csrf
                        <input type="hidden" name="id" value="{{ ( old('id') ) ? old('id') : $user->id }}">
                        <br>                        
                        <div class="form-group row">
                            <label for="name" class="col-md-4 col-form-label text-md-right">{{ __('Name') }}</label>

                            <div class="col-md-6">
                                
                                <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ ( old('name') ) ? old('name') : $user->name }}" required autofocus>

                                @if ($errors->has('name'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        
                        <div class="form-group row">
                            <label for="screenName" class="col-md-4 col-form-label text-md-right">{{ __('Screen Name') }}</label>

                            <div class="col-md-6">
                                <input id="screenName" type="text" class="form-control{{ $errors->has('screenName') ? ' is-invalid' : '' }}" name="screenName" value="{{ ( old('screenName') ) ? old('screenName') : $user->screen_name }}" autofocus>

                                @if ($errors->has('screenName'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('screenName') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="profileFileName" class="col-md-4 col-form-label text-md-right">{{ __('Profile Image') }}</label>

                            <div class="col-md-6">
                                <input id="profileFileName" type="file" class="form-control" name="profileFileName" value="{{ ( old("profileFileName") ) ? old("profileFileName") : $user->profile_filename }}">

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first("profileFileName") }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ ( old('email') ) ? old('email') : $user->email }}" required>

                                @if ($errors->has('email'))
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group row">         
                            <label for="description" class="col-md-4 col-form-label text-md-right">{{ __('Description') }}</label>

                            <div class="col-md-6">
                                <textarea id="description" rows="6" class="form-control" name="description">{{ $user->description }}</textarea>   
                            </div>
                        </div>                  
                     
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-lg btn-primary">
                                    {{ __('Update') }}
                                </button>

                                <a href="{{ url('home') }}" class="btn btn-lg btn-primary">Close</a>
                                
                            </div>
                        </div>
                        <br>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection