@extends('layouts.master')
@section('retweet')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <br><br><br><br>
                <div class="card-header text-white bg-primary"><h2>Retweet</h2></div>

                <div class="card-body bg-info">
                    <form method="POST" action="{{ url('/tweet') }}">
                        @csrf
                        <br>
                        <input type="hidden" name="user_id" value="{{ $user->id }}">
                        <br>

                        <div class="form-group row mb-0">
                            <div class="col-md-6 col-md-offset-1">
                                <h4> {{ Auth::user()->name }} @ {{ Auth::user()->screen_name }}</h4>                                
                            </div>
                        </div>

                        <div class="form-group row mb-1">
                            <div class="col-md-8 col-md-offset-2">

                                <textarea id="tweetText" rows="10" class="form-control" name="tweetText"
                                    onkeydown="limitText(this.form.tweetText,this.form.countdown,280);" onkeyup='limitText(this.form.tweetText,this.form.countdown,280);'>{{ $tweet->tweet_text }}</textarea>
                                <input readonly type="text" name="countdown" size="3" value="{{ 280 - strlen( $tweet->tweet_text) }}"> characters remaining
                                
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-12">
                                <button type="submit" class="btn btn-lg btn-primary">
                                    Save
                                </button>
                                <a href="{{ url('home') }}" class="btn btn-lg btn-primary">Close</a>                                
                            </div>
                        </div>
                        <br>
                    </form>

                    <script type="text/javascript">
                        function limitText(limitField, limitCount, limitNum) {
                            if (limitField.value.length > limitNum) {
                                limitField.value = limitField.value.substring(0, limitNum);
                            } else {
                                limitCount.value = limitNum - limitField.value.length;
                            }
                        }
                    </script>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection