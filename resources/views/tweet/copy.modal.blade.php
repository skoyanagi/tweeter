<div class="modal fade" id="newTweet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">New Tweet</h4>
            </div>
            <form action="{{ route('tweet.store','userId')}}" method="post">                    
                {{csrf_field()}}
                <div class="modal-body">
                    <input type="hidden" name="user_id" id="user_id" value="">
                    <div class="form-group row mb-1">
                        <div class="col-md-8 col-md-offset-2">                            
                            <textarea id="tweetText" rows="10" class="form-control" name="tweetText"
                                onkeydown="limitText(this.form.tweetText,this.form.countdown,280);" onkeyup='limitText(this.form.tweetText,this.form.countdown,280);'></textarea>
                            <h5 class="text-center"><input readonly class="text-center" type="text" id="countdown" name="countdown" size="3" value="280"> characters remaining</h5>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-warning">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="editTweet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Edit Tweet</h4>
            </div>
            <form action="{{ route('tweet.update','tweet_id') }}" method="post">                
                {{csrf_field()}}
                <div class="modal-body">
                    <input type="hidden" name="tweet_id" id="tweet_id" value="">
                    <div class="form-group row mb-1">
                        <div class="col-md-8 col-md-offset-2">                            
                            <textarea id="tweetText" rows="10" class="form-control" name="tweetText" onkeydown="limitText(this.form.tweetText,this.form.countdown,280);" onkeyup="limitText(this.form.tweetText,this.form.countdown,280);">
                            </textarea>
                            <h5 class="text-center"><input readonly class="text-center" type="text" id="countdown" name="countdown" size="3" value="280"> characters remaining</h5>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
    
<div class="modal modal-danger fade" id="deleteTweet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title text-center" id="myModalLabel">Delete Confirmation</h4>
            </div>
            <form action="{{ route('tweet.destroy','tweet_id')}}" method="delete">
                {{method_field('delete')}}
                {{csrf_field()}}
                <div class="modal-body">
                    <p class="text-center">
                        Are you sure you want to delete this tweet?<br>
                        All Comments will also be deleted.
                    </p>
                    <input type="hidden" name="tweet_id" id="tweet_id" value="">
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-success" data-dismiss="modal">Cancel</button>
                    <button type="submit" class="btn btn-warning">Delete</button>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="modal fade" id="retweet" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Retweet</h4>
            </div>
            <form action="{{ route('tweet.retweet') }}" method="post">                
                {{csrf_field()}}
                <div class="modal-body">
                    <input type="hidden" name="tweet_id" id="tweet_id" value="">
                    <input type="hidden" name="user_id" id="user_id" value="">
                    <input type="hidden" name="retweet_ind" id="retweet_ind" value="1">                    
                    <div class="form-group row mb-1">                        
                        <div class="col-md-8 col-md-offset-2">                            
                            <textarea id="tweetComment" rows="5" class="form-control" name="tweetComment" placeholder="Please enter your comments" onkeydown="limitText(this.form.tweetComment,this.form.countdown,280);" onkeyup="limitText(this.form.tweetComment,this.form.countdown,280);"></textarea>
                        </div>
                        <div class="col-md-8 col-md-offset-2">
                            <br>                            
                            <input type="text" readonly id="tweetUserName" class="form-control" name="tweetUserName" value="">
                            <textarea rows="4" wrap="hard" readonly id="tweetText" class="form-control" name="tweetText"></textarea>
                            <h5 class="text-center"><input readonly class="text-center" type="text" id="countdown" name="countdown" size="3" value=""> characters remaining</h5>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Save Changes</button>
                </div>
            </form>
        </div>
    </div>
</div>
